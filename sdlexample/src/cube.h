#include <SDL_opengles2.h>

extern "C" {
    #include "util.h"
}

class Cube
{
public:
    Cube();
    ~Cube();

    void Rotate();
    void Draw();

private:
    GLuint _modelId;
    GLuint _colorId;

    GLuint _program;
    GLuint _positionLoc;
    GLuint _colorLoc;
    GLuint _mvpLoc;

    glMatrix _projectionMatrix;
    glMatrix _modelviewMatrix;
    glMatrix _mvpMatrix;

    const GLfloat _vertices[108] = {
        // front
        -0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,

        // right
        0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, 0.5f, -0.5f,
        0.5f, 0.5f, -0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,

        // back
        0.5f, 0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        // left
        -0.5f, 0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,
        -0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, 0.5f,

        // top
        -0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, -0.5f,
        0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,

        // bottom
        -0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        0.5f, -0.5f, -0.5f};

    const GLfloat _colors[144] = {
        // front (red)
        1.0f, 0, 0, 1.0f,
        1.0f, 0, 0, 1.0f,
        1.0f, 0, 0, 1.0f,
        1.0f, 0, 0, 1.0f,
        1.0f, 0, 0, 1.0f,
        1.0f, 0, 0, 1.0f,

        // right (green)
        0, 1.0f, 0, 1.0f,
        0, 1.0f, 0, 1.0f,
        0, 1.0f, 0, 1.0f,
        0, 1.0f, 0, 1.0f,
        0, 1.0f, 0, 1.0f,
        0, 1.0f, 0, 1.0f,

        // back (dark red)
        0.5f, 0, 0, 1.0f,
        0.5f, 0, 0, 1.0f,
        0.5f, 0, 0, 1.0f,
        0.5f, 0, 0, 1.0f,
        0.5f, 0, 0, 1.0f,
        0.5f, 0, 0, 1.0f,

        // left (dark green)
        0, 0.5f, 0, 1.0f,
        0, 0.5f, 0, 1.0f,
        0, 0.5f, 0, 1.0f,
        0, 0.5f, 0, 1.0f,
        0, 0.5f, 0, 1.0f,
        0, 0.5f, 0, 1.0f,

        // top (blue)
        0, 0, 1.0f, 1.0f,
        0, 0, 1.0f, 1.0f,
        0, 0, 1.0f, 1.0f,
        0, 0, 1.0f, 1.0f,
        0, 0, 1.0f, 1.0f,
        0, 0, 1.0f, 1.0f,

        // bottom (dark blue)
        0, 0, 0.5f, 1.0f,
        0, 0, 0.5f, 1.0f,
        0, 0, 0.5f, 1.0f,
        0, 0, 0.5f, 1.0f,
        0, 0, 0.5f, 1.0f,
        0, 0, 0.5f, 1.0f};

    // Vertex shader
    const GLchar *_vertexSource =
        "precision mediump float;"
        "uniform mat4 u_mvpMat;"
        "attribute vec4 a_position;"
        "attribute vec4 a_color;"
        "varying vec4 v_color;"
        "void main()"
        "{"
        "    gl_Position = u_mvpMat * a_position;"
        "    v_color = a_color;"
        "}";

    // Fragment shader
    const GLchar *_fragmentSource =
        "varying lowp vec4 v_color;"
        "void main()"
        "{"
        "    gl_FragColor = v_color;"
        "}";
};