#include <SDL.h>
#include <SDL_opengles2.h>

#include <math.h>
#include <stdexcept>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "engine.h"

using namespace std;

Engine::Engine()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        throw std::runtime_error(SDL_GetError());

    _window = SDL_CreateWindow(NULL, 0, 0, 1024, 1024, SDL_WINDOW_OPENGL);

    if (_window == 0)
        throw std::runtime_error(SDL_GetError());

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);

    SDL_GLContext glc = SDL_GL_CreateContext(_window);

    glClearDepthf(1.0f);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    glEnable(GL_CULL_FACE);
    glViewport(0, 0, 1024, 1024);
    
    _cube = new Cube();
}

Engine::~Engine()
{
    delete _cube;

    SDL_Quit();
}

bool Engine::EventHandler()
{
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            return false;
            break;
        }
    }

    return true;
}

void Engine::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _cube->Rotate();
    _cube->Draw();

    SDL_GL_SwapWindow(_window);
}