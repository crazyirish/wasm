#include <SDL_opengles2.h>

typedef struct
{
    GLfloat mat[4][4];
} glMatrix;

void loadIdentity(glMatrix *result);
void multMatrix(glMatrix *result, glMatrix *srcA, glMatrix *srcB);
void scaleMatrix(glMatrix *result, GLfloat sx, GLfloat sy, GLfloat sz);
void rotationMatrix(glMatrix *result, GLfloat angle, GLfloat x, GLfloat y, GLfloat z);
void frustumMatrix(glMatrix *result, float left, float right, float bottom, float top, float nearZ, float farZ);
