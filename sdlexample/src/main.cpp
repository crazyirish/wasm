#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <exception>
#include <stdio.h>

#include "engine.h"

using namespace std;

#ifdef __EMSCRIPTEN__
void mainLoop(void *arg)
{
    Engine &engine = *static_cast<Engine *>(arg);

    if (engine.EventHandler())
    {
        engine.Render();
    }
    else
    {
        emscripten_cancel_main_loop();
    }
}
#endif

int main(int argc, char **argv)
{
    try
    {
        Engine engine;

#ifdef __EMSCRIPTEN__
        emscripten_set_main_loop_arg(&mainLoop, &engine, -1, 1);
#else
        while (engine.EventHandler())
            engine.Render();
#endif
    }
    catch (exception &e)
    {
        fprintf(stderr, "%s", e.what());
        return 1;
    }

    return 0;
}
