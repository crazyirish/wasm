#include <SDL_opengles2.h>
#include <SDL_stdinc.h>

#include <math.h>
#include <stdlib.h>

#include "cube.h"

Cube::Cube()
{
    // create shaders
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &_vertexSource, NULL);
    glCompileShader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &_fragmentSource, NULL);
    glCompileShader(fragmentShader);

    // compile & link
    _program = glCreateProgram();
    glAttachShader(_program, vertexShader);
    glAttachShader(_program, fragmentShader);
    glLinkProgram(_program);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // get shader variable positions (to bind later)
    _positionLoc = glGetAttribLocation(_program, "a_position");
    _colorLoc = glGetAttribLocation(_program, "a_color");
    _mvpLoc = glGetUniformLocation(_program, "u_mvpMat");

    // create vertex buffer
    glGenBuffers(1, &_modelId);
    glBindBuffer(GL_ARRAY_BUFFER, _modelId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(_vertices), _vertices, GL_STATIC_DRAW);

    // create color buffer
    glGenBuffers(1, &_colorId);
    glBindBuffer(GL_ARRAY_BUFFER, _colorId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(_colors), _colors, GL_STATIC_DRAW);

    // setup projection matrix
    GLfloat aspect = 1.0f;
    GLfloat near = -2.0f;
    GLfloat far = 2.0f;
    GLfloat yFOV = 75.0f;
    GLfloat height = tanf(yFOV / 360.0f * M_PI) * near;
    GLfloat width = height * aspect;

    loadIdentity(&_projectionMatrix);
    frustumMatrix(&_projectionMatrix, -width, width, -height, height, near, far);
    scaleMatrix(&_projectionMatrix, 1.0f, aspect, 1.0f);

    // init model view matrix
    loadIdentity(&_modelviewMatrix);
}

Cube::~Cube()
{
    glDeleteBuffers(1, &_modelId);
    glDeleteBuffers(1, &_colorId);
    glDeleteProgram(_program);
}

void Cube::Rotate()
{
    // rotate the model view
    rotationMatrix(&_modelviewMatrix, 1.0f, -1.0f, -1.0f, 0.0f);

    // multiply the model view with the projection matrix to get the position in 3d space
    multMatrix(&_mvpMatrix, &_modelviewMatrix, &_projectionMatrix);
}

void Cube::Draw()
{
    // activate shader program
    glUseProgram(_program);

    // activate vertex buffer and bind to shader
    glEnableVertexAttribArray(_positionLoc);
    glBindBuffer(GL_ARRAY_BUFFER, _modelId);
    glVertexAttribPointer(_positionLoc, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

    // activate color buffer and bind to shader
    glEnableVertexAttribArray(_colorLoc);
    glBindBuffer(GL_ARRAY_BUFFER, _colorId);
    glVertexAttribPointer(_colorLoc, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

    // copy the model view position matrix into the shader program
    glUniformMatrix4fv(_mvpLoc, 1, false, &_mvpMatrix.mat[0][0]);

    // draw!
    glDrawArrays(GL_TRIANGLES, 0, 36);

    glDisableVertexAttribArray(_positionLoc);
    glDisableVertexAttribArray(_colorLoc);
}