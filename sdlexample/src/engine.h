#include <SDL.h>
#include <SDL_opengles2.h>

#include "cube.h"

class Engine
{
public:
    Engine();
    ~Engine();

    bool EventHandler();
    void Render();

private:
    SDL_Window *_window;
    Cube *_cube;
};