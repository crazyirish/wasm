import { readFileSync } from "fs";

const x = 1.23456789;

console.log("Native...");

let result = x;

const start = new Date().getTime();

for (let i = 0; i < 2000000000; i++)
    result = Math.sin(result);

const stop = new Date().getTime();

const elapsed = stop - start;

console.log("Result: " + result);
console.log("Execution time: " + elapsed + "ms\n");



console.log("WASM...");
WebAssembly.compile(readFileSync("../lib/dist/lib.wasm"))
    .then(wasm => WebAssembly.instantiate(wasm))
    .then(instance => {
        const exported_function = instance.exports.exported_function as (x: number) => number;

        const start = new Date().getTime();

        const result = exported_function(x);
        
        const stop = new Date().getTime();

        const elapsed = stop - start;

        console.log("Result: " + result);
        console.log("Execution time: " + elapsed + "ms");
    });
