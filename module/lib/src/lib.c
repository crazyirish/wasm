#include <emscripten.h>
#include <math.h>

EMSCRIPTEN_KEEPALIVE
double exported_function(double x) {
    for (int i = 0; i < 2000000000; i++)
        x = sin(x);

    return x;
}