
(function () {
    let x = 1.23456789;

    let el = document.body;
    el.innerHTML = "Native...<br>";

    setTimeout(() => {
        let result = x;

        const start = new Date().getTime();

        for (let i = 0; i < 2000000000; i++)
            result = Math.sin(result);
    
        const stop = new Date().getTime();
    
        const elapsed = stop - start;
    
        el.innerHTML += "Result: " + result + "<br>";
        el.innerHTML += "Execution time: " + elapsed + "<br><br>";
    

        
        el.innerHTML += "WASM...<br>";
        WebAssembly.instantiateStreaming(fetch("lib.wasm"))
            .then(wasm => {
                const exported_function = wasm.instance.exports.exported_function;
    
                const start = new Date().getTime();
    
                const result = exported_function(x);
    
                const stop = new Date().getTime();
    
                const elapsed = stop - start;
    
                el.innerHTML += "Result: " + result + "<br>";
                el.innerHTML += "Execution time: " + elapsed + "<br>";
            });
    }, 100);
})();
