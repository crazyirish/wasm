# 1. What is Web Assembly - https://webassembly.org
- javascript
	- pros:
		- have direct access to the DOM
	- cons:
		- runs in a VM in the browser (Chrome V8)
		- large payloads (even minified js)
		- has to be parsed and interpreted at runtime

- wasm
	- pros:
		- compiles into byte code (similar to C# or Java), smaller and faster
		- runs with near-native performance
		- can utilize existing libraries
	- cons:
		- no direct access to the DOM, javascript is required to interop/marshal data

  
  

# 2. Setup a C/C++ dev environment - https://emscripten.org/

Checkout emscripten sdk
```
git clone https://github.com/emscripten-core/emsdk.git
./emsdk/emsdk install latest
./emsdk/emsdk activate latest
```

Setup bash environment
```
source ./emsdk/emsdk_env.sh
```

or, setup powershell environment
```
./emsdk/emsdk.bat
```

Verify installation
```
emcc --version
```
  

# 3. Hello World

```
cd hello && mkdir dist
emcc src/hello.c -s WASM=1 -o dist/hello.html
emrun dist/hello.html
```

  

# 4. Creating a library

```
cd ../module && mkdir lib/dist
emcc lib/src/lib.c -s WASM=1 -o lib/dist/lib.wasm --no-entry
```

  

# 5. Loading a WASM module
- Browser (need to copy lib/dist/lib.wasm to web/dist)

	```
	cd web
	cp ../lib/dist/lib.wasm ./
	npm i
	npm run start
	```
	Navigate to https://localhost:8080

- NodeJS

	```
	cd ../node && mkdir dist
	npm i
	npm run build
	npm run start
	```

  

# 6. Advanced example - OpenGL 3D rendering in the browser

- Windows (no tooling)

	```
	emcc src/main.cpp src/engine.cpp src/cube.cpp src/util.c -o dist/sdlexample.html -s WASM=1 -s USE_SDL=2
	emrun dist/sdlexample.html
	```

  

- Linux

	```
	sudo apt update
	sudo apt install build-essential cmake
	emmake cmake .
	emmake make
	emrun dist/sdlexample.html --browser="/mnt/c/Program Files (x86)/Google/Chrome/Application/chrome.exe"
	```

# 7. Awesome WASM...
Lots of cool things already done with WASM: https://github.com/mbasso/awesome-wasm
